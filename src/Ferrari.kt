class Ferrari:CocheDeportivo {
    override fun arranque() {
        println("Arranca el Ferrari")
    }

    override fun apagarMotor() {
        println("Apago Motor Ferrari")
    }

    override fun acelerar() {
        println("Ferrari acelera en 3.1 seg")
    }

    override fun frenar() {
        println("Ferrari frena en 4 seg")
    }
}