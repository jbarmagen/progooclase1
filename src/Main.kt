
fun main(){
    var j:Mujer=Mujer("Julia",20,false)
    var yony:Hombre=Hombre("Yony",36,1.84,false)
    var carol:Mujer=Mujer("Carol",19,false)

    var coche1:CocheDeportivo=Ferrari()
    var coche2:CocheDeportivo=Mercedes()

    j.asignar_coche(coche1)
    yony.asignar_coche(coche2)

    coche1.arranque()
    coche1.acelerar()
    coche2.arranque()
    coche2.acelerar()

}