class Mercedes:CocheDeportivo {

    override fun acelerar() {
        println("Mercedes acelera en 3.5 seg")
    }

    override fun apagarMotor() {
        println("Mercedes apaga el motor")
    }

    override fun arranque() {
        println("Mercedes arranca el motor")
    }

    override fun frenar() {
        println("Mercedes frena en 5 seg")
    }
}