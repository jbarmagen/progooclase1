open class Humano {

    var sColor_pelo:String=""
    var sColor_ojos:String=""
    var iEdad:Int=0
    var dAltura:Double=0.0
    var sNombre:String=""

    var co_Mi_coche: CocheDeportivo? =null

    constructor(nombre:String, color_pelo:String){
        sNombre=nombre
        sColor_pelo=color_pelo
    }

    fun asignar_coche(c:CocheDeportivo){
        co_Mi_coche=c
        //mis_coches.add(c)
    }

    fun cumpleanos(){
        iEdad=iEdad+1
        println(sNombre+" ha cumplido años")
    }

    open fun puede_conducir():Boolean{
        if(iEdad>=18){
            return true
        }
        else{
            return false
        }
    }

}